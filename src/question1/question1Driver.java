/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question1;

/**
 *
 * @author mfaizmzaki
 */
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.fs.Path;
import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;


public class question1Driver {
    
    public static class question1Mapper extends Mapper<LongWritable, Text, question1Format, IntWritable>{
        
        private final question1Format wordPair = new question1Format();
        private final IntWritable one = new IntWritable(1);
        private final IntWritable totalCount = new IntWritable(1);
        
        
        
        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            
            String[] tokens = value.toString().split("\\s+");
            if (tokens.length > 1) {
                for (int i = 0; i < tokens.length; i++) {
                    tokens[i] = tokens[i].replaceAll("\\W+","");
                    
                    if(i+1 == tokens.length) continue;
                    else{
                    wordPair.setWord(tokens[i]);
                    wordPair.setNeighbor(tokens[i+1].replaceAll("\\W",""));
                    context.write(wordPair, one);    
                    }
                    wordPair.setNeighbor("*");
                    context.write(wordPair, totalCount);
                }
            }
        }    
        
    }
    
    
    
    public static class question1Reducer extends Reducer<question1Format, IntWritable, question1Format, DoubleWritable> {
        private final DoubleWritable totalCount = new DoubleWritable();
        private final DoubleWritable relativeCount = new DoubleWritable();
        private final Text currentWord = new Text("NOT_SET");
        private final Text flag = new Text("*");
        
        @Override
        protected void reduce(question1Format key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            if (key.getNeighbor().equals(flag)) {
                if (key.getWord().equals(currentWord)) {
                    totalCount.set(totalCount.get() + getTotalCount(values));
                } else {
                    currentWord.set(key.getWord());
                    totalCount.set(0);
                    totalCount.set(getTotalCount(values));
                }
            } else {
                int count = getTotalCount(values);
                relativeCount.set((double) count / totalCount.get());
                context.write(key, relativeCount);
            }
        }
        private int getTotalCount(Iterable<IntWritable> values) {
            int count = 0;
            for (IntWritable value : values) {
                count += value.get();
            }
            return count;
        }
    }
    

    
    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
    
    //Starting point of execution
    Configuration conf = new Configuration(); 
    String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs(); // get all args
    if (otherArgs.length != 2) {
      System.err.println("Usage: WordCount <in> <out>");
      System.exit(2);
    }

    // create a job with name "wordcount"
    Job job = new Job(conf, "pairsAlgo");
    job.setJarByClass(question1Driver.class);
    
    job.setMapperClass(question1Mapper.class);
    job.setReducerClass(question1Reducer.class);
    job.setCombinerClass(question1Combiner.class);
    job.setPartitionerClass(question1Partitioner.class);
     
    // set output key type   
    job.setOutputKeyClass(question1Format.class);
    // set output value type
    job.setOutputValueClass(IntWritable.class);
    //set the HDFS path of the input data
    FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
    // set the HDFS path for the output
    FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));

    //Wait till job completion
    System.exit(job.waitForCompletion(true) ? 0 : 1);  
        
    }
    
}
