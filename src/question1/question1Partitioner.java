/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question1;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Partitioner;

/**
 *
 * @author mfaizmzaki
 */
 public class question1Partitioner extends Partitioner<question1Format,IntWritable> {

    @Override
    public int getPartition(question1Format wordPair, IntWritable intWritable, int numPartitions) {
        return Math.abs(wordPair.getWord().hashCode()) % numPartitions;
    }
}
